const tabsCaptionLi = document.querySelectorAll('.tabs__caption li');
for (let i = 0; i < tabsCaptionLi.length; i++) {
    tabsCaptionLi[i].addEventListener('click', switchTab);
}

function switchTab() {
    const ul = this.parentElement;
    const allListItems = ul.children;
    const arrayListItems = Array.prototype.slice.call(allListItems);
    console.log(arrayListItems);
    arrayListItems.forEach(function (item) {
        item.classList.remove('active');
    });
    this.classList.add('active');
    const tabsContent = document.querySelectorAll('.tabs .tabs__content');
    for (let i = 0; i < tabsContent.length; i++) {
        tabsContent[i].classList.remove('active');
    }
    // console.log(arrayListItems.indexOf(this));
    const position = arrayListItems.indexOf(this);
    tabsContent[position].classList.add('active');
}

$(document).ready(function () {
    $(".item").hide();
    $(".item").slice(0, 12).show();
    $("#btnLoadMore").click(function () {
        const imageType = $('.tabs2_links.active').data('category');
        $(`.item${imageType}:hidden`).slice(0, 12).show();
        $('#btnLoadMore').show();
        if ($(`.item${imageType}:hidden`).length < 1) {
            $('#btnLoadMore').hide();
        }
    });

    $('.tabs2_links').click(function () {
        $(this).addClass('active').siblings().removeClass("active");
        const imageType = $(this).data("category");
        $('.item').hide();
        $(`.item${imageType}`).slice(0, 12).show();
        if ($(`.item${imageType}:hidden`).length < 1) {
            $('#btnLoadMore').hide();
        } else {
            $('#btnLoadMore').show();
        }
    })
});

$(document).ready(function () {
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.slider-nav',
    });
    $('.slider-nav').slick({
        autoplay: true,
        autoplaySpeed: 7000,
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        focusOnSelect: true,
        variableWidth: true,
        prevArrow: "<i class=\"fas fa-chevron-left prev fz-fa\"></i>",
        nextArrow: "<i class=\"fas fa-chevron-right next fz-fa\"></i>",
    });
});

$(document).ready(function () {
    $('.grid').hide();
    $('.grid').slice(0, 8).show();
    $('#show-grid').click(function () {
        $('.grid:hidden').slice(0, 9).show();
        if ($('.grid:hidden').length < 1) {
            $('#show-grid').hide();
        }
        let msnry = new Masonry(elem, {
            itemSelector: '.grid-item',
            columnWidth: '.grid-sizer',
            gutter: 22,
            percentPosition: true,
        });
    });
    let elem = document.querySelector('.container_grid');
    let msnry = new Masonry(elem, {
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        gutter: 22,
        percentPosition: true,
    });
});

